package Drones;

import java.io.Serializable;

import javafx.scene.paint.Color;
/**
 * class for size, abstract methods, drone information and id.
 */
public abstract class Drone implements Serializable {
	protected int id;							//ID used for drones.
    protected double x, y, radian;		//drone sizes and direction
    static int ballID = 0;		// Unique ID for drones

    droneArena dro; //droneArena object
    Drone() {
        this(100, 100, 10);
    }
    /**
     * constructor for drone in x, y and rad.
     * @param xy
     * @param yx
     * @param rad
     */
    Drone(double xy, double yx, double rad) {
        this.x = xy;
        this.y = yx;
        radian = rad;
        id = ballID++;			//id increases the unique ID
    }
    /**
     * getter function return x of drone
     * @return x
     */
    public double getX() { 
    	return x; 
    	}
    /**
     * getter function return y of drone
     * @return y
     */
    public double getY() { 
    	return y; 
    	}
    /**
     * getter function, return rad of drone
     * @return radian
     */
    public double getRad() { 
    	return radian; 
    	}
    /**
     * set the drone at defined locations
     * @param nx
     * @param ny
     */
    public void setXY(double nx, double ny) {
        x = nx;
        y = ny;
    }
    /**
     * getter function return drone id
     * @return id
     */
    public int getID() {
    	return id; 
    	}
    /**
     * draw Drone image on defiend x and y locations.
     * @param can
     */
    public void drawDrone(consoleCanvas can) {
    	can.drawCar(x, y); //draw the drone
        
    }
    protected String getStrType() {
        return "Car"; 
    }
    /**
     * toString for the drone location
     */
    public String toString() {
        return getStrType()+" at "+Math.round(x)+", "+Math.round(y);
    }
    /**
     * abstract function for drone movement
     */
    protected abstract void adjustDrone();
    /**
     * abstract function for checking drone in droneArena
     * @param dro
     */
    protected abstract void checkDrone(droneArena dro);
  
    /**
     * checking if hit 
     * @param ox
     * @param oy
     * @param or
     * @return true
     */
    public boolean hit(double ox, double oy, double or) {
        return (ox-x)*(ox-x) + (oy-y)*(oy-y) < (or+radian)*(or+radian);
    }		// hit is made if dist between ball and ox,oy < ist rad + or

    /** has the drone touched the other drone?
     * @param drone
     * @return true
     */
    public boolean hitting (Drone drone) {
        return hit(drone.getX(), drone.getY(), drone.getRad());
    }
}
