package Drones;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Class to create the Arena to contain the objects and arrayList
 */
public class droneArena implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	double xSize, ySize;	// size of arena
    protected ArrayList<Drone> list; // array list of all balls in arena
    /**
     * construct an arena of the defined size.
     */
    droneArena() {
        this(1200, 900);
    }
    /**
     * position of the drones in the list, to be added onto the defined drone arena created at specified x and y sizes.
     * @param xSize
     * @param ySize
     */
    droneArena(double xSize, double ySize){
        this.xSize = xSize;
        this.ySize = ySize;
        
        list = new ArrayList<Drone>();					// list of all drones and obstacles.
        list.add(new DronesBounce(xSize/2, ySize*0.8, 10, 45, 6));	//add the drones
        list.add(new Enemy(xSize/2, ySize/5, 35));			// adds enemy drone
        list.add(new Player(xSize/2, ySize-20, 40));		// adds player paddle
        list.add(new Obstacle(xSize/20, 1*ySize/5, 25)); // adds obstacles
        list.add(new Obstacle(xSize/8, 1*ySize/5, 25));
        list.add(new Obstacle(xSize/5, 1*ySize/3, 40));
        list.add(new Obstacle(xSize/3, 1*ySize/10, 25));
        list.add(new Obstacle(xSize, 1*ySize/5, 25));
        list.add(new Obstacle(2*xSize/4, 1*ySize/5, 25));
        list.add(new Obstacle(2*xSize/8, 1*ySize/5, 25));
        list.add(new Obstacle(xSize-100, 1*ySize/5, 25));
        list.add(new Obstacle(xSize-70, 1*ySize/5, 25));
        list.add(new Obstacle(xSize-30, 1*ySize/5, 25));
    }
    /**
     * getter function, x size of drone arena
     * @return xSize
     */
    public double getX() {
        return xSize;
    }
    /**
     * getter function, y size of drone arena
     * @return ySize
     */
    public double getY() {
        return ySize; 
    }
    /**
     * arena drawn onto consoleCanvas
     * @param can
     */
    public void drawArena(consoleCanvas can) {
        for (Drone d : list) d.drawDrone(can); //show all drones in the list
    }
    /**
     * check all drones.
     */
    public void checkDrones() {
        for (Drone d : list) d.checkDrone(this);
    }
    /**
     * movement for all drones
     */
    public void adjustDrones() {
        for (Drone d : list) d.adjustDrone();
    }
    /**
     * set the locations of the paddle
     * @param x
     * @param y
     */
    public void setPaddle(double x, double y) {
        for (Drone d : list)
            if (d instanceof Player) d.setXY(x, y);
    }
    /**
     * return list of strings in an arrayList for all individual drones in the arraylist for the drones
     * @return ans
     */
    public ArrayList<String> addAll() {
        ArrayList<String> ans = new ArrayList<String>();		//set up empty ArrayList
        for (Drone d : list) ans.add(d.toString());			//String for each Drone
        return ans;										
    }
    /**
     * checks the angle of the drone and if it hits a wall or other drones or enemy.
     * @param x		
     * @param y				
     * @param rad			
     * @param ang			
     * @param notID			
     * @return ans
     */
    public double CheckDroneAngle(double x, double y, double rad, double ang, int notID) {
        double ans = ang;
        if (x < rad || x > xSize - rad) ans = 180 - ans;
        // if drone tried to hit anything, mirror the angle thus opposite direction
        if (y < rad || y > ySize - rad) ans = - ans;
        // if try to go off top or bottom, set mirror angle thus opposite direction

        for (Drone d : list)
            if (d.getID() != notID && d.hit(x, y, rad)) ans = 180*Math.atan2(y-d.getY(), x-d.getX())/Math.PI;
        // check all drones
       

        return ans;	
    }

    /**
     * check if the enemy drone has been touched by a drone
     * @param drone
     * @return 	true 
     */
    public boolean checkHit(Drone drone) {
        boolean ans = false;
        for (Drone d : list)
            if (d instanceof DronesBounce && d.hitting(drone)) ans = true;
        //check all drones
        return ans;
    }
    
    public boolean inBorder(Drone drone) {
    	return !(drone.getX() - drone.getRad() < 0 || drone.getY() - drone.getRad() < 0 || drone.getX() + drone.getRad() > xSize || drone.getY() + drone.getRad() > ySize);
    }
    /**
     * adds a drone at the specified coordinates, updating arrayList 
     * 
     */
    public void addDrone() {
        list.add(new DronesBounce(xSize/2, ySize*0.8, 10, 60, 5));
    }
}
