package Drones;

import javafx.scene.paint.Color;

/**
 *class to create the paddle functionality
 */
public class Player extends Drone {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**Set ]paddle size xy at yx, rad position on the canvas.
     * @param xy
     * @param yx
     * @param rad
     */
    public Player(double xy, double yx, double rad) {
        super(xy, yx, rad);
           
    }
 
    /**
     * show the paddle image at specified x and y.
     * @param can
     */
    public void drawDrone(consoleCanvas can) {
        can.drawPaddle(x, y);
    }

    /**
     *  to check paddle
     */
    @Override
    protected void checkDrone(droneArena dro) {
        //null
    }

    /**
     *  to adjust paddle
     */
    @Override
    protected void adjustDrone() {
        //null
    }
    /**
     *  string for the paddle
     */
    protected String getStrType() {
        return "Player";
    }
}
