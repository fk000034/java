package Drones;



/**
 * class to create drones 
 */
public class DronesBounce extends Drone {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	droneArena dro;
    double ang, Speed;			// angle and speed of travel
    
    /** Drones at size ix, iy, ir moving at ang and speed.
     * @param ix
     * @param iy
     * @param ir
     * @param ang
     * @param speed
     */
    public DronesBounce(double ix, double iy, double ir, double ang, double Speed) {
        super(ix, iy, ir);
        this.ang = ang;
        this.Speed = Speed;
    }
    
    /**
     * draw drone image at x and y
     * @param can
     */
    public void drawDrone(consoleCanvas can) {
    	
    	can.drawDrone(x, y);
    }

    /**
     * change direction of drone when touched by an obstacle, enemy or other drones.
     * @param dro
     */
    @Override
    protected void checkDrone(droneArena dro) {
        ang = dro.CheckDroneAngle(x, y, radian, ang, id);
    }

    /**
     * drone movement
     */
    @Override
    protected void adjustDrone() {
        double radAngle = ang*Math.PI/180;		// angle in rads
        x += Speed * Math.cos(radAngle);		//new coordinates
        y += Speed * Math.sin(radAngle);		
    }
    /**
     * return string defining the drone
     */
    protected String getStrType() {
        return "Drone"; //return string as Drone
    }

}
