package Drones;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * class to use the javafx library and show the gui
 */
public class Interface extends Application {
    private static consoleCanvas can;
    private AnimationTimer timer;								// timer for the animation and simulation
    private VBox vbox;										// vBox used for outputting the drone information to the right side
    private static droneArena dro;

    private void theAbout() {
        Alert alert = new Alert(AlertType.CONFIRMATION);	//Alert message as a confirmation
        alert.setTitle("Information");	// title set to information about the application and how to use.
        alert.setHeaderText("Tutorial"); // header title saying tutorial for the users.
        alert.setContentText("Drone Paddle game alongside an inbuilt simulator. \nClick start to get Started.\n"
        		+ "AddDrones to add drones onto the canvas. \n"  
        		+ "Full Screen to enter full Screen. \n"
        		+ "Click stop to stop the simulation or game. \n"
        		+ "Restart to redraw the canvas and reset the application. \n"
        		+ "Simulate to start a drone simulator in an empty canvas. \n"
        		+ "Exit to exit the application. \n"); //Sentences for the app
        alert.showAndWait();	// stays shown till user exits the alert.
    }
    /**
     * How the paddle reacts when the mouse is used, thus an interactive paddle
     * @param canvas
     */
    void movePaddle (Canvas canvas) {
        canvas.addEventHandler(MouseEvent.MOUSE_DRAGGED, 		// When mouse is clicked and held thus drag.
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent e) {
                        dro.setPaddle(e.getX(), e.getY());
                        drawWorld();							//keep drawing world as user drags the paddle around the canvas.
                        drawStatus();
                    }
                });
    }
    /**
     * Menubar set up with different sub menus.
     * @return MenuBar
     */
    public MenuBar toolbar() {
		
        MenuBar menuBar = new MenuBar();
   		
   		Menu menu = new Menu("File"); //Toolbar menu called File
   		MenuItem saveFile1 = new MenuItem("Save");
   		saveFile1.setOnAction(new EventHandler<ActionEvent>() {
               public void handle(ActionEvent e) {					
               	saveFile(); //what happens when save is pressed
               }
   		});
   		
   		MenuItem LoadF = new MenuItem("Load");					//sub menu has Save button
           LoadF.setOnAction(new EventHandler<ActionEvent>() {
               public void handle(ActionEvent e) {					//what happens when load is pressed
                   loadFile(); //load function is loaded.
               }
           });
           
           Menu helpMenu = new Menu("About");
           MenuItem help = new MenuItem("Help");
           help.setOnAction(new EventHandler<ActionEvent>() {
               public void handle(ActionEvent e) {					//what happens when the help button is pressed located as a sub menu under About
                   theAbout();
               }
           });
           
           menu.getItems().addAll(saveFile1, LoadF);
           helpMenu.getItems().addAll(help);
           
           menuBar.getMenus().addAll(menu, helpMenu);
    
           
           return menuBar;
   	}
    /**
     * Hbox set up for the location and showcase of the buttons in a horizontal look 
     * @return
     */
    private HBox setButtons() {
        Button startSim = new Button("Start");			// button to start the simulation
        startSim.setOnAction(e -> timer.start());

        Button stop = new Button("Stop");				// button to stop the simulation
        stop.setOnAction(e -> timer.stop());

        Button add = new Button("Add Drone");				// button to add another drone/ball
        add.setOnAction(e -> {
        dro.addDrone(); 
        drawWorld();
        });
      
        
		Button exit = new Button("Exit");
		exit.setOnAction(e -> System.exit(0));
		
		Button restart = new Button("Restart");
		restart.setOnAction(e -> {
			dro = new droneArena(1200, 900);								// set up arena
	        drawWorld();
		});
		
		Button simulate = new Button("Simulate");
		simulate.setOnAction(e ->{
			can.clearCanvas();
			dro.list.clear();
			//dro.addDrone();
		});
        // now add these buttons + labels to a HBox
        return new HBox(startSim, add, stop, restart ,simulate ,exit);
    }

    /**
     * show the score at defined positions.
     * @param x
     * @param y
     * @param score
     */
    public void showScore (double x, double y, int score) {
        can.drawText(x, y, Integer.toString(score)); //shows score on the enemy when ever the drone touches it.
    }
    /**
     * create the canvas with the drones alongside it.
     */
    public void drawWorld () {
        can.clearCanvas();		//Clear the canvas				
        dro.drawArena(can); //draw the arena with paramenter consoleCanvas
    }

    /**
     *  show drone information within the list as a label.
     *  @return VBox
     */
    public VBox drawStatus() {
        vbox.getChildren().clear();					//clear vbox
        ArrayList<String> alldrones = dro.addAll(); //add all within the ArrayList.
        for (String s : alldrones) {
            Label l = new Label(s); 
            vbox.getChildren().add(l);	//add the label
        }
        return new VBox(); //return a new vbox
    }
    


    /**
     *  Main function to output the drones, canvas, frame and buttons. 
     */
    @Override
    public void start(Stage stage) throws Exception {
        // TODO Auto-generated method stub
        stage.setTitle("Paddle Drone");
        BorderPane panel = new BorderPane();
        panel.setPadding(new Insets(10, 20, 10, 20));

        panel.setTop(toolbar());							//Set menuBar at the top

        Group root = new Group();					//Using group and then adding canvas into the group.
        Canvas canvas = new Canvas(1200, 900);
        root.getChildren().add(canvas);
        panel.setCenter(root);							//Load the group which contains canvas into the center using a borderPane.

        can = new consoleCanvas(canvas.getGraphicsContext2D(), 1200, 900); //creating consoleCanvas with defined x and y sizes

        movePaddle(canvas);											//set up paddle onto canvas

        dro = new droneArena(1200, 900);					//create drone arena
        drawWorld(); //draw world

        timer = new AnimationTimer() {						//Initialise timer
            public void handle(long Timer) {			//using timer as parameter as a long
                dro.checkDrones();						
                dro.adjustDrones();					//moving drones
                drawWorld();									
                drawStatus();			//drone information
            }
        };

        vbox = new VBox();
        vbox.getChildren().addAll(drawStatus());
        vbox.setAlignment(Pos.CENTER);			//vbox set in the center
        vbox.setPadding(new Insets(0, 0, 0, 50));	
        panel.setLeft(vbox);		//setting the vbox on the left using the panel

        Button fullScreen = new Button("Full Screen");
        fullScreen.setOnAction(e -> stage.setFullScreen(true));
        HBox box = new HBox();
		box.setAlignment(Pos.CENTER);
		box.setPadding(new Insets(0, 0, 30, 0));
		
        box.getChildren().addAll(fullScreen, setButtons());
		
		panel.setBottom(box);	// set bottom pane with buttons
		
		
		
        Scene scene = new Scene(panel, 1200, 900);							//how the overall application is then shown
        panel.prefHeightProperty().bind(scene.heightProperty());
        panel.prefWidthProperty().bind(scene.widthProperty());

        stage.setScene(scene); //setting the scene.
        stage.setMaximized(true); //application starts maximised.
        stage.setResizable(true); //it is resizable
        stage.centerOnScreen();
        stage.getIcons().add(new Image("drone.png")); //icon set on the left and in the task bar for the application
        stage.show(); //show to the user.


    }
    /**
     *  loading the simulation from a saved text file
     */
    public void loadFile() {
    	String url = "saved.txt";
    	try {
    		ObjectInputStream loader = new ObjectInputStream(new FileInputStream(url));
    		dro = (droneArena) loader.readObject();
    		loader.close();
    		drawWorld();										// redraw the world
    	} catch(Exception e) {
    		System.out.println(e);
    	}
    }
    /**
     * saving the simulation to a text file
     */
    public void saveFile() {
    	String url = "saved.txt";
    	try {
    		ObjectOutputStream saver = new ObjectOutputStream(new FileOutputStream(url));
    		saver.writeObject(dro);
    		saver.close();
    	} catch(Exception e) {
    		System.out.println(e);
    	}
    }

    /**
     * launching the javaFx stage
     * @param args
     */
    public static void main(String[] args) {
        Application.launch(args);			

    }

}
