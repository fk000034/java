package Drones;

import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.scene.text.TextAlignment;

/**
 *  Main class to display the actual canvas where the drones will operate
 */
public class consoleCanvas {
	int ySize = 1200; //size of y on canvas
    int xSize = 900; //size of x on canvas
    GraphicsContext gc; //graphics context object
    Image drone = new Image("drone.png"); //loading image from url
    Image obstacle = new Image("obstacle.png"); // loading obstacle image from url
    Image paddle = new Image("paddle.png"); //loading paddle image from url
    Image obstacle2 = new Image("obstacle2.jpg"); //loading second obstacle image from url
    
    //Image drone = new Image("file:///C:/Users/dzza1/eclipse-workspace/DroneCwk/src/drone.png"); //loading image from url
    //Image obstacle = new Image("file:///C:/Users/dzza1/eclipse-workspace/DroneCwkGui/src/obstacle.png"); // loading obstacle image from url
    //Image paddle = new Image("file:///C:/Users/dzza1/eclipse-workspace/DroneCwkGui/src/paddle.png"); //loading paddle image from url
    //Image obstacle2 = new Image("file:///C:/Users/dzza1/eclipse-workspace/DroneCwkGui/src/obstacle2.jpg"); //loading second obstacle image from url this code for the jar file to work on my system as jar file will not work without a specified image url.

    /**
     * constructor to initialise graphics context, xSize and ySize
     * @param g
     * @param x
     * @param y
     */
    public consoleCanvas(GraphicsContext g, int x, int y) {
        gc = g;
        xSize = x;
        ySize = y;
    }
    /**
     * getter function
     * @return xSize
     */
    public int getXCanvasSize() {
        return xSize;
    }
    /**
     * getter function
     * @return ySize
     */
    public int getYCanvasSize() {
        return ySize;
    }

    /**
     * emptying canvas and clearing.
     */
    public void clearCanvas() {
        gc.setFill(Color.WHITE); //canvas colour
        gc.fillRect(0, 0, xSize, ySize); //filling the canvas with defined colour in defined x and y size
        gc.setStroke(Color.BLACK); //set border to black
        gc.strokeRect(0, 0, xSize, ySize); //create border from defined colour and size
    }
    
    public void setFill (String col) {
        gc.setFill(Color.valueOf(col));
    }
    /**
     * Draw image obstacle as a car on x and y with the size of 80 x 60
     * @param x
     * @param y
     */
    public void drawCar(double x, double y) {
        	gc.drawImage(obstacle, x, y, 80, 60);
    }  
    /**
     * Draw image drone as a drone on x and y with the size of 25, 35
     * @param x
     * @param y
     */
    
    public void drawDrone(double x, double y) {
			gc.drawImage(drone, x, y, 25, 35);
	}
    /**
     * Draw image paddle as a wooden plank on x and y with the size of  60 x 30
     * @param x
     * @param y
     */
    public void drawPaddle(double x, double y) {
		gc.drawImage(paddle, x, y, 60, 30);
}
    /**
     * Draw image obstacle2 as a brick wall on x and y with the size of 30 x 35
     * @param x
     * @param y
     */
    public void drawObstacle(double x, double y) {
    	gc.drawImage(obstacle2, x, y, 30, 35);
    }
    /**
     * write the string s at position x,y for the score to be shown on the car.
     * @param x
     * @param y
     * @param s
     */
    public void drawText (double x, double y, String s) {
        gc.setTextAlign(TextAlignment.CENTER);	//text set in the center
        gc.setTextBaseline(VPos.CENTER);							
        gc.setFill(Color.BLACK); //colour of the text.
        gc.fillText(s, x, y);	//finalising the text.
    }

    /**
     * Int i at defined x,y positions and then shown as a integer to a String.
     * @param x
     * @param y
     * @param i
     */
    public void showInt (double x, double y, int i) {
        drawText (x, y, Integer.toString(i)); //draw the text.
    }
}
