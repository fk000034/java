package Drones;

/**
 * class for the enemy Drone and how it will work
 */
public class Enemy extends Drone {
    private int score;
    private int direction = 1; 
    private double speed = 6;

    /**
     * score to be set up for the enemy drone
     * @param xy
     * @param yx
     * @param rad
     */
    public Enemy(double xy, double yx, double rad) {
        super(xy,yx,rad);
        score = 0; //score set to 0
    }
    /**
     * return string of enemy
     */
    protected String getStrType() {
        return "enemy";
    }

    /**
     * function to check drone
     * @param droneArena
     */
    @Override
    protected void checkDrone(droneArena dro) {
        if (dro.checkHit(this)) {
        	score++; //if drone touches enemy, enemy increase by 1.
        }
        if (dro.inBorder(this) == false) {
        	direction = -direction;
        }
    }
    /**
     * draw enemy with score alongside.
     */
    public void drawDrone(consoleCanvas can) {
        super.drawDrone(can);
        can.showInt(x, y, score);
    }

    /**
     * when drone touches, change direction.
     */
    @Override
    protected void adjustDrone() {
        x += speed * direction;				
    }
  
}
