package Drones;


/**
 * class for the blocking walls
 */
public class Obstacle extends Drone {
	private static final long serialVersionUID = 1L;
	/**
     * creating obstacles of those sizes.
     * @param xy
     * @param yx
     * @param rad
     */
    public Obstacle(double xy, double yx, double rad) {
        super(xy,yx,rad);
    }
    
    /**
     * draw the obstacle image
     * @param can
     */
    public void drawDrone(consoleCanvas can) {
        
    	can.drawObstacle(x, y);
    	
    }
    /** 
     * String of the obstacle location, size .
     */
    
    protected String getStrType() {
        return "Obstacle";
    }

    /** 
     * checks obstacle
     */
    protected void checkDrone(droneArena dro) {
        //null
    }

    /** 
     * adjusts obstacle
     */
    protected void adjustDrone() {
       //null
    }
}
